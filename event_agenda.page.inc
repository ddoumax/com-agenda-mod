<?php

/**
 * @file
 * Contains event_agenda.page.inc.
 *
 * Page callback for Event agenda entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Event agenda templates.
 *
 * Default template: event_agenda.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_event_agenda(array &$variables) {
  // Fetch EventAgenda Entity Object.
  $event_agenda = $variables['elements']['#event_agenda'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

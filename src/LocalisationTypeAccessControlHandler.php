<?php

namespace Drupal\com_agenda_mod;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the localisation type entity.
 *
 * @see \Drupal\com_agenda_mod\Entity\EventType.
 */
class LocalisationTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\com_agenda_mod\Entity\EventAgendaInterface $entity */

    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view localisation type entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit localisation type entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete localisation type entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add localisation type entities');
  }


}

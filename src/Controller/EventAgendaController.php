<?php

namespace Drupal\com_agenda_mod\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\com_agenda_mod\Entity\EventAgendaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EventAgendaController.
 *
 *  Returns responses for Event agenda routes.
 */
class EventAgendaController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Event agenda revision.
   *
   * @param int $event_agenda_revision
   *   The Event agenda revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($event_agenda_revision) {
    $event_agenda = $this->entityTypeManager()->getStorage('event_agenda')
      ->loadRevision($event_agenda_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('event_agenda');

    return $view_builder->view($event_agenda);
  }

  /**
   * Page title callback for a Event agenda revision.
   *
   * @param int $event_agenda_revision
   *   The Event agenda revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($event_agenda_revision) {
    $event_agenda = $this->entityTypeManager()->getStorage('event_agenda')
      ->loadRevision($event_agenda_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $event_agenda->label(),
      '%date' => $this->dateFormatter->format($event_agenda->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Event agenda.
   *
   * @param \Drupal\com_agenda_mod\Entity\EventAgendaInterface $event_agenda
   *   A Event agenda object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(EventAgendaInterface $event_agenda) {
    $account = $this->currentUser();
    $event_agenda_storage = $this->entityTypeManager()->getStorage('event_agenda');

    $langcode = $event_agenda->language()->getId();
    $langname = $event_agenda->language()->getName();
    $languages = $event_agenda->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $event_agenda->label()]) : $this->t('Revisions for %title', ['%title' => $event_agenda->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all event agenda revisions") || $account->hasPermission('administer event agenda entities')));
    $delete_permission = (($account->hasPermission("delete all event agenda revisions") || $account->hasPermission('administer event agenda entities')));

    $rows = [];

    $vids = $event_agenda_storage->revisionIds($event_agenda);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\com_agenda_mod\EventAgendaInterface $revision */
      $revision = $event_agenda_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $event_agenda->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.event_agenda.revision', [
            'event_agenda' => $event_agenda->id(),
            'event_agenda_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $event_agenda->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.event_agenda.translation_revert', [
                'event_agenda' => $event_agenda->id(),
                'event_agenda_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.event_agenda.revision_revert', [
                'event_agenda' => $event_agenda->id(),
                'event_agenda_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.event_agenda.revision_delete', [
                'event_agenda' => $event_agenda->id(),
                'event_agenda_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['event_agenda_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}

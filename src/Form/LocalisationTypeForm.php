<?php

namespace Drupal\com_agenda_mod\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LocalisationTypeForm.
 */
class LocalisationTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $localisation_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $localisation_type->label(),
      '#description' => $this->t("Label for the Localisation type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $localisation_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\com_agenda_mod\Entity\LocalisationType::load',
      ],
      '#disabled' => !$localisation_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $localisation_type = $this->entity;
    $status = $localisation_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Localisation type.', [
          '%label' => $localisation_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Localisation type.', [
          '%label' => $localisation_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($localisation_type->toUrl('collection'));
  }

}

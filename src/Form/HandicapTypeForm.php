<?php

namespace Drupal\com_agenda_mod\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HandicapTypeForm.
 */
class HandicapTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $handicap_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $handicap_type->label(),
      '#description' => $this->t("Label for the Handicap type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $handicap_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\com_agenda_mod\Entity\HandicapType::load',
      ],
      '#disabled' => !$handicap_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $handicap_type = $this->entity;
    $status = $handicap_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Handicap type.', [
          '%label' => $handicap_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Handicap type.', [
          '%label' => $handicap_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($handicap_type->toUrl('collection'));
  }

}

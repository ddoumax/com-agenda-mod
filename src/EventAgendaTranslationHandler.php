<?php

namespace Drupal\com_agenda_mod;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for event_agenda.
 */
class EventAgendaTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}

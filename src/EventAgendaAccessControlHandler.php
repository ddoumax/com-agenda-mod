<?php

namespace Drupal\com_agenda_mod;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Event agenda entity.
 *
 * @see \Drupal\com_agenda_mod\Entity\EventAgenda.
 */
class EventAgendaAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\com_agenda_mod\Entity\EventAgendaInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished event agenda entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published event agenda entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit event agenda entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete event agenda entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add event agenda entities');
  }


}

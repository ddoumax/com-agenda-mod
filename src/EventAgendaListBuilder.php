<?php

namespace Drupal\com_agenda_mod;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Event agenda entities.
 *
 * @ingroup com_agenda_mod
 */
class EventAgendaListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = [
      'data' => 'Id',
      'field' => 't.id',
    ];
    $header['name'] = [
      'data' => 'Name',
      'field' => 't.name',
    ];
    $header['event_type'] = [
      'data' => 'Event Type',
      'field' => 't.event_type',
      'sort' => 'asc',
    ];
    $header['field_ea_periode'] = [
      'data' => 'Period',
      'field' => 't.periode',
      'sort' => 'asc',
    ];
    //$header['periode'] = $this->t('Periode');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\com_agenda_mod\Entity\EventAgenda $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.event_agenda.edit_form',
      ['event_agenda' => $entity->id()]
    );
    $row['event_type'] = $entity->getEventTypeLabel();
    $row['periode'] = $entity->getPeriode();
    return $row + parent::buildRow($entity);
  }

  protected function getEntityIds() {
    $query = \Drupal::entityQuery($this->entityTypeId);
    $request = \Drupal::request();

    $field = $request->get('name') ?? 0;
    if ($field) {
      $query->condition('name', $field, 'CONTAINS');
    }

    $field = $request->get('status') ?? 'publie';
    if ($request->get('status')) {
      $query->condition('status', $request->get('status') == 'publie' ? 1 : 0);
    }

    foreach (['event_type', 'public_type', 'localisation_type', 'handicap_type'] as $item) {
      $field = $request->get($item) ?? 0;
      if ($field) {
        $query->condition($item, $field);
      }
    }

    if ($this->limit) {
      $query->pager($this->limit);
    }

    $order = $request->get('order');
    if ($order) {
      $sort = $request->get('sort');
      foreach ($this->buildHeader() as $name => $field) {
        if (is_array($field) && $field['data'] == $order) {
          $header = [$name => $field + [
              'specifier' => $name,
              'sort' => $sort ?? $field['sort'] ?? 'asc',
            ]];
        }
      }
      if ($header) {
        $query->tableSort($header);
      }
    }
    else {
      $query->sort('name', 'asc');
    }

    $query->accessCheck(TRUE);

    return $query->execute();
  }

  public function render() {
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\com_agenda_mod\Form\EventAgendaFilterForm');
    $build += parent::render();
    return $build;
  }
}

<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Event agenda entities.
 *
 * @ingroup com_agenda_mod
 */
interface EventAgendaInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Event agenda name.
   *
   * @return string
   *   Name of the Event agenda.
   */
  public function getName();

  /**
   * Sets the Event agenda name.
   *
   * @param string $name
   *   The Event agenda name.
   *
   * @return \Drupal\com_agenda_mod\Entity\EventAgendaInterface
   *   The called Event agenda entity.
   */
  public function setName($name);

  /**
   * Gets the Event agenda creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Event agenda.
   */
  public function getCreatedTime();

  /**
   * Sets the Event agenda creation timestamp.
   *
   * @param int $timestamp
   *   The Event agenda creation timestamp.
   *
   * @return \Drupal\com_agenda_mod\Entity\EventAgendaInterface
   *   The called Event agenda entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Event agenda revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Event agenda revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\com_agenda_mod\Entity\EventAgendaInterface
   *   The called Event agenda entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Event agenda revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Event agenda revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\com_agenda_mod\Entity\EventAgendaInterface
   *   The called Event agenda entity.
   */
  public function setRevisionUserId($uid);

  public function getEventTypeKey();

  public function getEventTypeLabel();


}

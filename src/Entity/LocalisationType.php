<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Localisation type entity.
 *
 * @ConfigEntityType(
 *   id = "localisation_type",
 *   label = @Translation("Localisation type"),
 *   handlers = {
 *     "access" = "Drupal\com_agenda_mod\LocalisationTypeAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\com_agenda_mod\LocalisationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\com_agenda_mod\Form\LocalisationTypeForm",
 *       "edit" = "Drupal\com_agenda_mod\Form\LocalisationTypeForm",
 *       "delete" = "Drupal\com_agenda_mod\Form\LocalisationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\com_agenda_mod\LocalisationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "localisation_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   admin_permission = "administer localisation type entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/localisation_type/{localisation_type}",
 *     "add-form" = "/admin/structure/localisation_type/add",
 *     "edit-form" = "/admin/structure/localisation_type/{localisation_type}/edit",
 *     "delete-form" = "/admin/structure/localisation_type/{localisation_type}/delete",
 *     "collection" = "/admin/structure/localisation_type"
 *   }
 * )
 */
class LocalisationType extends ConfigEntityBase implements LocalisationTypeInterface {

  /**
   * The Localisation type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Localisation type label.
   *
   * @var string
   */
  protected $label;

  /**
   * @return string
   */
  public function toString() {
    return $this->label;
  }
}

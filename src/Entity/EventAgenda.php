<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\link\LinkItemInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Event agenda entity.
 *
 * @ingroup com_agenda_mod
 *
 * @ContentEntityType(
 *   id = "event_agenda",
 *   label = @Translation("Event agenda"),
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   handlers = {
 *     "storage" = "Drupal\com_agenda_mod\EventAgendaStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\com_agenda_mod\EventAgendaListBuilder",
 *     "views_data" = "Drupal\com_agenda_mod\Entity\EventAgendaViewsData",
 *     "translation" = "Drupal\com_agenda_mod\EventAgendaTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\com_agenda_mod\Form\EventAgendaForm",
 *       "add" = "Drupal\com_agenda_mod\Form\EventAgendaForm",
 *       "edit" = "Drupal\com_agenda_mod\Form\EventAgendaForm",
 *       "delete" = "Drupal\com_agenda_mod\Form\EventAgendaDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\com_agenda_mod\EventAgendaHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\com_agenda_mod\EventAgendaAccessControlHandler",
 *   },
 *   base_table = "event_agenda",
 *   data_table = "event_agenda_field_data",
 *   revision_table = "event_agenda_revision",
 *   revision_data_table = "event_agenda_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer event agenda entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/event_agenda/{event_agenda}",
 *     "add-form" = "/admin/structure/event_agenda/add",
 *     "edit-form" = "/event_agenda/{event_agenda}/edit",
 *     "delete-form" = "/event_agenda/{event_agenda}/delete",
 *     "version-history" = "/event_agenda/{event_agenda}/revisions",
 *     "revision" = "/event_agenda/{event_agenda}/revisions/{event_agenda_revision}/view",
 *     "revision_revert" = "/event_agenda/{event_agenda}/revisions/{event_agenda_revision}/revert",
 *     "revision_delete" = "/event_agenda/{event_agenda}/revisions/{event_agenda_revision}/delete",
 *     "translation_revert" = "/event_agenda/{event_agenda}/revisions/{event_agenda_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/event_agenda",
 *   },
 *   field_ui_base_route = "event_agenda.settings"
 * )
 */
class EventAgenda extends EditorialContentEntityBase implements EventAgendaInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the event_agenda owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventTypeKey() {
    return $this->get('event_type')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventTypeLabel() {
    return $this->get('event_type')->entity ? $this->get('event_type')->entity->label() : t('Type introuvable');
  }

  /**
   * {@inheritdoc}
   */
  public function getPeriode() {
    if($this->hasField('field_ea_periode')) {
      $date = new \DateTime();
      $date->setTimestamp($this->get('field_ea_periode')->value);

      $dateEnd = new \DateTime();
      $dateEnd->setTimestamp($this->get('field_ea_periode')->end_value);

      if($date->format('Y-m-d') == $dateEnd->format('Y-m-d')) {
        return $date->format('Y-m-d') . ' de ' . $date->format('H:i') . ' à ' . $dateEnd->format('H:i');
      }

      return $date->format('Y-m-d') .  ' au ' . $dateEnd->format('Y-m-d');
    }
    else
    {
      return "";
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $weight = -50;

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t("Name of Event"))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['menu_parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Menu parent'))
      ->setDescription(t("A renseigner pour le fil d'ariane"))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'menu_link_content')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date_afficher'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Afficher les dates de début / fin'))
      ->setDescription(t('Afficher les dates de début / fin'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(true)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date_debut_publication'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Début de publication'))
      ->setDescription(t('Début de publication'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'datetime_type' => 'date',
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'medium',
        ],
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date_fin_publication'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Fin de publication'))
      ->setDescription(t('Fin de publication'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'datetime_type' => 'date',
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'medium',
        ],
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['event_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t("Type d'événement"))
      ->setRequired(TRUE)
      ->setDescription(t("Type d'événement"))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'event_type')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sous_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Sous-Type'))
      ->setDescription(t('Sous-Type of the Event.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['public_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type de Public'))
      ->setDescription(t('Type de Public.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'public_type')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['resume'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Résumé'))
      ->setDescription(t('Résumé.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of the Event.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['photo'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Photo'))
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'credentialing_providers',
        'file_extensions' => 'png gif jpg jpeg',
        'title_field' => TRUE,
        'title_field_required' => FALSE,
        'alt_field' => TRUE,
        'alt_field_required' => FALSE,
      ])
      ->setDescription(t('Photo Evénement.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => $weight++,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayOptions('form', array(
        'type' => 'image_image',
        'weight' => $weight++,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setReadOnly(TRUE);

    $fields['visuel'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Visuel'))
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'credentialing_providers',
        'file_extensions' => 'png gif jpg jpeg',
        'title_field' => TRUE,
        'title_field_required' => FALSE, //optional
        'alt_field' => TRUE,
        'alt_field_required' => FALSE, //optional
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Visuel Evénement.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => $weight++,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayOptions('form', array(
        'type' => 'image_image',
        'weight' => $weight++,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setReadOnly(TRUE);

    $fields['pere'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Evènement Père'))
      ->setDescription(t('Evènement Père'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['afficher_resume_pere'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t("Afficher le résumé de l'évenement père (si un évènement est sélectionné)"))
      ->setDescription(t("Afficher le résumé de l'évenement père (si un évènement est sélectionné)"))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['lieu'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Lieu'))
      ->setDescription(t('Lieu of the Event.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['localisation_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Localisation dans le musée'))
      ->setDescription(t('Localisation dans le musée'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'localisation_type')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['handicap_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t("Accessibilité"))
      ->setDescription(t('Handicap Type.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'handicap_type')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['handicap_affichage'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t("Accessibilité Affichage"))
      ->setDescription(t('Handicap Affichage Evénement.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['organisateur'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Organisateur'))
      ->setDescription(t('Organisateur Evénement.'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entree_libre'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Entrée Libre'))
      ->setDescription(t('Entrée Libre Evénement.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tarif'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Tarif'))
      ->setDescription(t('Tarif of the Event.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['renseignements'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Renseignements'))
      ->setDescription(t('Renseignements of the Event.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['lien_reservation'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Lien Réservation'))
      ->setDescription(t('Lien Réservation.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'Name' => DRUPAL_DISABLED
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('view', array(
        'type' => 'file_generic',
        'weight' => $weight++,
      ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['lien_billetterie'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Lien billetterie'))
      ->setDescription(t('Lien billetterie.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'Name' => DRUPAL_DISABLED
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('view', array(
        'type' => 'file_generic',
        'weight' => $weight++,
      ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['en_savoir_plus'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Lien En Savoir Plus'))
      ->setDescription(t('Lien En Savoir Plus.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'Name' => DRUPAL_DISABLED
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('view', array(
        'type' => 'file_generic',
        'weight' => $weight++,
      ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['documents'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Document à télécharger'))
      ->setDescription(t('Document à télécharger.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setSettings([
        'file_extensions' => 'txt, pdf',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'file_default',
        'weight' => $weight++,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'file_generic',
        'weight' => $weight++,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['document_title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Document Title'))
    ->setDescription(t('Document Title'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 60,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 23,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 23,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['mettre_en_avant'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Afficher en page d\'accueil'))
      ->setDescription(t("Concerne l'affichage des évènements hors Expositions en page d'accueil - Incompatible avec l'option \"Afficher en tête de la page d'accueil\"."))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['prioritaire'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t("Afficher en tête de la page d'accueil"))
      ->setDescription(t("Concerne l'affichage des évènements hors Expositions en page d'accueil - Incompatible avec l'option \"Afficher en page d'accueil\""))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['a_la_une'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('A la une'))
      ->setDescription(t('A la une.'))
      ->setDefaultValue(3)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['a_voir_aussi_images'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('A voir aussi (Images)'))
      ->setDescription(t("Affiche l'événement dans la zone avec images du bloc \"A voir aussi\""))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['a_voir_aussi_liste'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('A voir aussi (Liste)'))
      ->setDescription(t("Affiche l'événement dans la zone avec images du bloc \"A voir aussi\""))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Event agenda is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => $weight++,
      ]);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Event agenda entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => $weight++,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}

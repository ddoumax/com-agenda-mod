<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Handicap type entity.
 *
 * @ConfigEntityType(
 *   id = "handicap_type",
 *   label = @Translation("Handicap type"),
 *   handlers = {
 *     "access" = "Drupal\com_agenda_mod\HandicapTypeAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\com_agenda_mod\HandicapTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\com_agenda_mod\Form\HandicapTypeForm",
 *       "edit" = "Drupal\com_agenda_mod\Form\HandicapTypeForm",
 *       "delete" = "Drupal\com_agenda_mod\Form\HandicapTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\com_agenda_mod\HandicapTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "handicap_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   admin_permission = "administer handicap type entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/handicap_type/{handicap_type}",
 *     "add-form" = "/admin/structure/handicap_type/add",
 *     "edit-form" = "/admin/structure/handicap_type/{handicap_type}/edit",
 *     "delete-form" = "/admin/structure/handicap_type/{handicap_type}/delete",
 *     "collection" = "/admin/structure/handicap_type"
 *   }
 * )
 */
class HandicapType extends ConfigEntityBase implements HandicapTypeInterface {

  /**
   * The Handicap type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Handicap type label.
   *
   * @var string
   */
  protected $label;

  /**
   * @return string
   */
  public function toString() {
    return $this->label;
  }
}

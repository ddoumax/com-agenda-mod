<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Event agenda entities.
 */
class EventAgendaViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
